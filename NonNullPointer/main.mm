//
//  main.cpp
//  NonNullPointer
//
//  Created by Andrey Mishanin on 10/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#include <iostream>
#include <unordered_set>

#import <Foundation/Foundation.h>

#include "NonNullPointer.h"

struct Pt {
  int x;
  int y;
  Pt(int x, int y) : x(x), y(y) { }
};

void test_Dereference();
void test_PassThrough();
void test_BoolConversion();
void test_ConstructionFromNullLiteral();
void test_UncheckedConstuctionFromNullable();
void test_ConstructionFromNullableTrap();
void test_NonNullableUnique();
void test_PassingNonNullableUnique();
void test_PassingAsRaw();
void test_ComparisonWithRaw();
void test_StoringInUnorderedSet();

// Helpers for testing passing non null pointer as argument
void takeNonNullableUniquePtr(NonNullUniquePtr<int>) {}
void takeRawObjCPtr(NSObject *) {}

template <typename T>
class TypeDebugger;

#define RevealType(T) auto __td = TypeDebugger<T>()

int main(int argc, const char * argv[]) {
  test_Dereference();
  test_PassThrough();
  test_BoolConversion();
  test_ConstructionFromNullLiteral();
  test_UncheckedConstuctionFromNullable();
  test_ConstructionFromNullableTrap();
  test_NonNullableUnique();
  test_PassingNonNullableUnique();
  test_PassingAsRaw();
  test_ComparisonWithRaw();
  test_StoringInUnorderedSet();
  return 0;
}

void test_ComparisonWithRaw() {
  auto o = [NSObject new];
  assert(o == makeNonNullable(o));
  assert(makeNonNullable(o) == o);
}

void test_StoringInUnorderedSet() {
  std::unordered_set<nn<Pt *>> s;
  s.emplace(makeNonNullable(new Pt(123, 123)));
}

void test_PassingAsRaw() {
  auto p = makeNonNullable([NSObject new]);
  takeRawObjCPtr(p);
}

void test_PassingNonNullableUnique() {
  takeNonNullableUniquePtr(makeNonNullableUnique<int>(1));
}

void test_NonNullableUnique() {
  std::cout << __FUNCTION__ << ":" << std::endl;
  auto p = makeNonNullableUnique<Pt>(2, 2);
  std::cout << p->x << std::endl;
  p->x = 42;
  std::cout << p->x << std::endl;
  *p = Pt {10, 10};
  std::cout << p->x << std::endl;
}

void test_ConstructionFromNullableTrap() {
  __unused NSObject *o = nil;
//  __unused auto p = makeNonNullable(o);
}

void test_UncheckedConstuctionFromNullable() {
  auto nullable = new int(7);
//  auto p = nn<int *>(nullable);
  delete nullable;

  __unused auto o = [NSObject new];
//  auto p = nn<NSObject *>(o);
}

void test_ConstructionFromNullLiteral() {
//  auto p1 = nn<int *>(nil);
  auto p2 = makeNonNullable(new int(7));
//  p2 = nullptr;
//  p2 = nil;
  delete static_cast<int *>(p2);
}

void test_BoolConversion() {
  auto p = makeNonNullable(new int(7));
//  if (p) { }
  delete static_cast<int *>(p);
}

void test_PassThrough() {
  std::cout << __FUNCTION__ << ":" << std::endl;
  auto p = makeNonNullable(new Pt(123, 123));
  p->x = 1;
  std::cout << p->x << std::endl;
  delete static_cast<Pt *>(p);

  auto po = makeNonNullable([NSObject new]);
  std::cout << [po hash] << std::endl;
}

void test_Dereference() {
  std::cout << __FUNCTION__ << ":" << std::endl;
  auto p = makeNonNullable(new int(7));
  std::cout << *p << std::endl;
  *p = 42;
  std::cout << *p << std::endl;
  delete static_cast<int *>(p);
}
