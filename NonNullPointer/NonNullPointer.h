//
//  NonNullPointer.h
//  NonNullPointer
//
//  Created by Andrey Mishanin on 10/12/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

#ifndef NonNullPointer_h
#define NonNullPointer_h

#include <cassert>
#include <cstdlib>
#include <memory>
#include <utility>

// Marker type and value
struct NullCheckMarkerType {
  NullCheckMarkerType() = delete;
};
static constexpr NullCheckMarkerType NullCheckMarker {};

namespace detail {
  // Smart ptrs
  template <typename T> struct PointeeType { using type = typename T::element_type; };
  // Raw ptrs
  template <typename Pointee> struct PointeeType<Pointee *> { using type = Pointee; };
}

template <typename PtrType>
class NonNullPointer {
public:
  using Pointee = typename detail::PointeeType<PtrType>::type;

  Pointee & operator*() const { return *ptr; }
  Pointee * operator->() const { return &*ptr; }

  // Conversion back to nullable
  operator const PtrType & () const & { return ptr; }
  operator PtrType && () && { return std::move(ptr); }

  // Conversion to bool doesn't make sense anymore
  operator bool () const = delete;

  // Constructors
  // Disallow construction and assignment from null literals
  NonNullPointer(std::nullptr_t) = delete;
  NonNullPointer & operator=(std::nullptr_t) = delete;

  // Disallow unchecked construction from nullable pointers
  NonNullPointer(PtrType) = delete;
  NonNullPointer & operator=(PtrType) = delete;

  // Checked construction
  explicit NonNullPointer(NullCheckMarkerType, const PtrType &arg)
  : ptr(arg) { assert(ptr != nullptr); }

  explicit NonNullPointer(NullCheckMarkerType, PtrType &&arg)
  : ptr(std::move(arg)) { assert(ptr != nullptr); }

  // Equality
  template <typename L, typename R>
  friend bool operator ==(const NonNullPointer<L> &lhs, const NonNullPointer<R> &rhs);

private:
  PtrType ptr;
};

template <typename Ptr>
using nn = NonNullPointer<Ptr>;

template <typename Ptr>
auto makeNonNullable(Ptr p) {
  assert(p != nullptr);
  if (p == nullptr) {
    std::abort();
  }

  using PtrType = typename std::remove_reference_t<decltype(p)>;
  return NonNullPointer<PtrType>(NullCheckMarker, std::move(p));
}

// Equality
template <typename L, typename R>
bool operator ==(const NonNullPointer<L> &lhs, const NonNullPointer<R> &rhs) {
  return lhs.ptr == rhs.ptr;
}

// Hashing
namespace std {
  template <typename T>
  struct hash<NonNullPointer<T>> {
    size_t operator()(const NonNullPointer<T> &ptr) const {
      return std::hash<T>{}(ptr);
    }
  };
}

// Unique ptr convenience
template <typename T> using NonNullUniquePtr = NonNullPointer<std::unique_ptr<T>>;

template <typename T, typename... Args>
NonNullUniquePtr<T> makeNonNullableUnique(Args &&... args) {
  return NonNullUniquePtr<T>(NullCheckMarker,
                             std::make_unique<T>(std::forward<Args>(args)...));
}

// Shared ptr convenience
template <typename T> using NonNullSharedPtr = NonNullPointer<std::shared_ptr<T>>;

template <typename T, typename... Args>
NonNullSharedPtr<T> makeNonNullableShared(Args &&... args) {
  return NonNullSharedPtr<T>(NullCheckMarker,
                             std::make_shared<T>(std::forward<Args>(args)...));
}

#endif /* NonNullPointer_h */
